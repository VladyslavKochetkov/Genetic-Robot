#include <iostream>
#include <vector>
#include <algorithm>
#include <thread>
#include <unistd.h>
#include <csignal>
#include <string.h>

using namespace std;

struct mapIndex{
    bool wall = false;
    bool battery = false;
};
struct gene {
    string start = "";
    string end = "";
};

mapIndex map[12][12];
int counter = 0;

class Robot {
private:
    int x, y;
    char facing;
    string currentSensor;

    string randomHelperSensor() {
        int random = rand() % 3;
        switch (random) {
            case 0:
                return "9";
            case 1:
                return "1";
            case 2:
                return "2";
        }
    }

    void getCurrentSenorData() { //9219 Wall North, Nothing Easy, Battery South, Wall West
        currentSensor = "";
        for (int i = 0; i < 4; i++) { // 9 = wall, 1 = battery, 2 = don't care / nothing
            switch (i) { //use switch because spaghetti code and im lazy as fuck
                case 0:
                    //cout << "RAN N" << endl;
                    if (map[x][y - 1].wall) {
                        currentSensor += "9";
                        break;
                    }else if (map[x][y - 1].battery) {
                        currentSensor += "1";
                        break;
                    }else
                        currentSensor += "2";
                    break;
                case 1:
                    //cout << "RAN E" << endl;
                    if (map[x + 1][y].wall) {
                        currentSensor += "9";
                        break;
                    }
                    else if (map[x + 1][y].battery) {
                        currentSensor += "1";
                        break;
                    }
                    currentSensor += "2";
                    break;
                case 2:
                    //cout << "RAN S" << endl;
                    if (map[x][y + 1].wall) {
                        currentSensor += "9";
                        break;
                    }
                    else if (map[x][y + 1].battery) {
                        currentSensor += "1";
                        break;
                    }
                    currentSensor += "2";
                    break;
                case 3:
                    //cout << "RAN W" << endl;
                    if (map[x - 1][y].wall) {
                        currentSensor += "9";
                        break;
                    }
                    else if (map[x - 1][y].battery) {
                        currentSensor += "1";
                        break;
                    }
                    currentSensor += "2";
                    break;
            }
        }
        //cout << currentSensor << endl;
    }

    void randomiseGenes() {
        for (int i = 0; i < 24; i++) { //run through each gene and randomise it
            bool allowM = true;
            bool allowP = true;
            allGenes[i].start = "";
            allGenes[i].start += randomHelperSensor();
            allGenes[i].start += randomHelperSensor();
            allGenes[i].start += randomHelperSensor();
            allGenes[i].start += randomHelperSensor();
            allGenes[i].end += randomHelperDirection();
            allGenes[i].end += randomHelperRotate();
            string handleRandom = randomHelperAction(allowM, allowP);
            if (handleRandom == "M") {
                allowM = false;
            } else if (handleRandom == "P") {
                allowP = false;
            }
            allGenes[i].end += handleRandom;
            allGenes[i].end += randomHelperAction(allowM, allowP);
            //cout << allGenes[i].start << " | " << allGenes[i].end << " (" << (allGenes[i].start.length() + allGenes[i].end.length()) << ")" << endl;
        }
    }
    string randomHelperDirection() {
        int random = rand() % 4;
        switch (random) {
            case 0:
                return "N";
            case 1:
                return "E";
            case 2:
                return "W";
            case 3:
                return "S";
        }
    }

    string randomHelperRotate() {
        int random = rand() % 2;
        switch (random) {
            case 0:
                return "RL";
            case 1:
                return "RR";
        }
    }

    string randomHelperAction(bool allowM, bool allowP) {
        if (allowM && allowP) {
            int random = rand() % 3;
            switch (random) {
                case 0:
                    return "M";
                case 1:
                    return "P";
                case 2:
                    return "X";
            }
        } else if (!allowM && allowP) {
            int random = rand() % 2;
            switch (random) {
                case 0:
                    return "P";
                case 1:
                    return "X";
            }
        } else if (allowM && !allowP) {
            int random = rand() % 2;
            switch (random) {
                case 0:
                    return "M";
                case 1:
                    return "X";
            }
        }
    }

    void parser(){
        //printAround(); //FOR DEBUGGING ONLY
        //get correct gene
        for(int i = 0; i < 24; i++){
            gene x = allGenes[i];
            if(x.start == currentSensor){
//                cout << x.start << " | " << x.end << endl;
                totalPoints++;
                if(facing == x.end.at(0)){
//                    cout << facing << " | " << x.end.at(0) << endl;
//                    cout << x.end.substr(1,2) << endl;
                    totalPoints++;
                    if(x.end.substr(1,2) == "RL"){
                        doRL();
                    }else if(x.end.substr(1,2) == "RR"){
                        doRR();
                    }
                    switch(x.end.at(3)){
                        case 'X':
                            //Literally do nothing
                            break;
                        case 'M':
                            doM();
                            break;
                        case 'P':
                            doP();
                            break;
                    }
                    switch(x.end.at(4)){
                        case 'X':
                            //Literally do nothing
                            break;
                        case 'M':
                            doM();
                            break;
                        case 'P':
                            doP();
                            break;
                    }
                }
            }
        }
    }

    void doRR(){
        switch(facing){
            case 'N':
                facing = 'E';
                break;
            case 'E':
                facing = 'S';
                break;
            case 'S':
                facing = 'W';
                break;
            case 'W':
                facing = 'N';
                break;
        }
    }

    void doRL(){
        switch(facing){
            case 'N':
                facing = 'W';
                break;
            case 'W':
                facing = 'S';
                break;
            case 'S':
                facing = 'E';
                break;
            case 'E':
                facing = 'N';
                break;
        }
    }

    void doP(){
        if(map[x][y].battery){
            energy+=5;
        }
    }

    void doM(){
        totalPoints++;
        switch(facing){
            case 'N':
                y--;
                if(map[this->x][y].wall){
                    y++; //stop climbing the wall you robot
                }
                break;
            case 'E':
                this->x++;
                if(map[this->x][y].wall){
                    this->x--;
                }
                break;
            case 'S':
                y++;
                if(map[this->x][y].wall){
                    y--;
                }
                break;
            case 'W':
                this->x--;
                if(map[this->x][y].wall){
                    this->x++;
                }
                break;
        }
    }

    void printAround(){ //FOR DEBUGGING ONLY
        cout << "Facing: " << facing << endl;
        cout << "Location: " << x << " " << y << endl;
        printNum(x - 1, y - 1);printNum(x, y - 1);printNum(x + 1, y - 1);cout << endl;
        printNum(x - 1, y);cout<<"X";printNum(x + 1, y);cout << endl;
        printNum(x - 1, y + 1);printNum(x, y + 1);printNum(x + 1, y + 1);cout << endl << endl;
    }

    void printNum(int x, int y){ //FOR DEBUGGING ONLY
        if(map[x][y].wall)
            cout << "2";
        else if(map[x][y].battery)
            cout << "1";
        else
            cout << "0";
    }
public:
    int energy = 10;
    int totalPoints = 0;
    gene allGenes[24];

    Robot() {
        do {
            x = (rand() % 10) + 1;
            y = (rand() % 10) + 1;
        } while (map[x][y].wall &&
                 !map[x][y].battery); //get another random spot if this spot is either a wall or not a battery
        int facingRandom = rand() % 4;
        switch (facingRandom) {
            case 0:
                facing = 'N';
                break;
            case 1:
                facing = 'W';
                break;
            case 2:
                facing = 'S';
                break;
            case 3:
                facing = 'E';
                break;
        }
        randomiseGenes();
    };

    Robot(gene allGenesReplace[24], bool toMutate) {
        do {
            x = (rand() % 10) + 1;
            y = (rand() % 10) + 1;
        } while (map[x][y].wall &&
                 !map[x][y].battery); //get another random spot if this spot is either a wall or not a battery
        int facingRandom = rand() % 4;
        switch (facingRandom) {
            case 0:
                facing = 'N';
                break;
            case 1:
                facing = 'W';
                break;
            case 2:
                facing = 'S';
                break;
            case 3:
                facing = 'E';
                break;
        };
        if(!toMutate) {
            for (int i = 0; i < 24; i++) {
                allGenes[i] = allGenesReplace[i];
            }
        }else{
            randomiseGenes();
            gene randomGene = allGenes[rand() % 24];
            int randomGeneFromPass = rand() % 24;
            int whichGene = rand() % 9;
            for(int i = 0; i < 24; i++){
                allGenes[i] = allGenesReplace[i];
            }
            if(whichGene < 4){
                string toReplace = ""; toReplace += randomGene.start.at(whichGene);
                allGenes[randomGeneFromPass].start.replace(whichGene, 1, toReplace);
            }else{
                whichGene -= 4;
                string toReplace = ""; toReplace += randomGene.end.at(whichGene);
                allGenes[randomGeneFromPass].end.replace(whichGene, 1, toReplace);
            }
        }
    };

    void getPOS() {
        cout << map[x][y].wall << endl << map[x][y].battery << endl;
        cout << "Location: " << x << " " << y << endl;
        cout << "Facing: " << facing << endl;
    }

    void forceLocation(int x, int y) { //purely for debugging purposes
        this->x = x;
        this->y = y;
    }



    void execute(){
        while(energy > 0){
            getCurrentSenorData();
            parser();
            energy--;
        }
//        cout << allGenes[1].start << allGenes[1].end << endl;
//        for(gene x : allGenes){
//            cout << currentSensor << " || " << x.start << endl;
//        }
//        if(totalPoints != 0){
//            cout << totalPoints << " (" << counter << ")" << endl;
//            counter++;
//        }
    }

    friend bool operator<(const Robot &a, const Robot &b){
        if(a.totalPoints < b.totalPoints){
            return true;
        }else{
            return false;
        }
    }
};

void setMap(){
    mapIndex defaultSet;
    for(int i = 0; i < 12; i++){
        for(int j = 0; j < 12; j++){
            map[i][j] = defaultSet;
        }
    }
    for(int i = 0; i < 12; i++){
        map[i][0].wall = true;
        map[0][i].wall = true;
        map[i][11].wall = true;
        map[11][i].wall = true;
    }

    int totalBatteries = 0;
    while(totalBatteries < 40){ //use 40 because that is 40% of the map
        int x = (rand() % 10) + 1, y = (rand() % 10) + 1;
        if(!map[x][y].wall & !map[x][y].battery){
            map[x][y].battery = true;
            totalBatteries++;
        }
    }

//    for(int i = 0; i < 12; i++){ //debug only
//        for(int j = 0; j < 12; j++){
//            if(i == 5 && j == 5){
//                cout << 'X';
//                continue;
//            }
//            if(map[i][j].battery || map[i][j].wall)
//                cout << 1;
//            else
//                cout << 0;
//        }
//        cout << endl;
//    }
}

bool showGenes = false, showGen = true, showBest = false;

void menu(){
    char input;
    do {
        cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
        cout << "Press 1 to show all genes at end of generation cycle (" << (showGenes ? "On)" : "Off)") << endl;
        cout << "Press 2 to show generation numbers (" << (showGen ? "On)" : "Off)") << endl;
        cout << "Press 3 to show best score for current generation (" << (showBest ? "On)" : "Off)") << endl;
        cout << "Press 4 to exit menu" << endl;
        input = cin.get();
        switch(input){
            case 49:
                showGenes = !showGenes;
                break;
            case 50:
                showGen = !showGen;
                break;
            case 51:
                showBest = !showBest;
                break;
        }
    }while(input != 52);
}

void my_handler(int sig){
    cout << sig << endl;
}

int main() {
    signal(SIGINT, my_handler);
    srand(time(NULL)); //random rand
    menu();
    vector<Robot> *allRobots = new vector<Robot>(200);
    int generation = 0;
    bool continueRunning = true;
    while(continueRunning) {
        setMap();
        for (int i = 0; i < 200; i++) {
            allRobots->at(i) = Robot();
            allRobots->at(i).execute();
        }
        sort(allRobots->begin(), allRobots->end());
        //purge and breed
        int parents = 199; //199 & 198
        int subCounter = 0;
        for (int i = 0; i < 100; i++) {
            if (subCounter == 2) {
                parents -= 2;
                subCounter = 0;
            }
            int whoGivesFirst = rand() % 2;
            gene toPass[24];
            switch (whoGivesFirst) {
                case 1:
                    if (rand() % 100 < 5) {
                        gene *allA = allRobots->at(parents).allGenes;
                        gene *allB = allRobots->at(parents - 1).allGenes;
                        for (int i = 0; i < 12; i++) {
                            toPass[i] = allA[i];
                        }
                        for (int i = 12; i < 24; i++) {
                            toPass[i] = allB[i];
                        }
                        /*TODO MUTATE */
                        allRobots->at(i) = Robot(toPass, true);
                    } else {
                        gene *allA = allRobots->at(parents).allGenes;
                        gene *allB = allRobots->at(parents - 1).allGenes;
                        for (int i = 0; i < 12; i++) {
                            toPass[i] = allA[i];
                        }
                        for (int i = 12; i < 24; i++) {
                            toPass[i] = allB[i];
                        }
                        allRobots->at(i) = Robot(toPass, false);
                    }
                    break;
                case 2:
                    if (rand() % 100 < 5) {
                        gene *allA = allRobots->at(parents).allGenes;
                        gene *allB = allRobots->at(parents - 1).allGenes;
                        for (int i = 0; i < 12; i++) {
                            toPass[i] = allB[i];
                        }
                        for (int i = 12; i < 24; i++) {
                            toPass[i] = allA[i];
                        }
                        /*TODO MUTATE */
                        allRobots->at(i) = Robot(toPass, true);
                    } else {
                        gene *allA = allRobots->at(parents).allGenes;
                        gene *allB = allRobots->at(parents - 1).allGenes;
                        for (int i = 0; i < 12; i++) {
                            toPass[i] = allB[i];
                        }
                        for (int i = 12; i < 24; i++) {
                            toPass[i] = allA[i];
                        }
                        allRobots->at(i) = Robot(toPass, false);
                    }
                    break;
            }
            subCounter++;
        }

        if(showGen)
            cout << "Generation: " << generation << "\n";
        if(showBest)
            cout << "Highest points:" << allRobots->at(199).totalPoints << "\n";
        if(showGenes) {
            cout << "Best Gene Set: \n";
            for (int i = 0; i < 24; i++) {
                cout << allRobots->at(199).allGenes[i].start << allRobots->at(199).allGenes[i].end << endl;
            }
        }
        generation++;
//        cout << "Press enter to continue, press 0 to exit: ";
//        int input = cin.get();
//        if(input == 10){
//            continue;
//        }
//        if(input == 48){
//            break;
//        }
    }
    return 0;
}