#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <time.h>

using namespace std;

bool debug = false;
struct mapIndex{
    bool hasBattery = false;
    bool hasWall = false;
};
mapIndex map[12][12];


class singleGene{
private:
    void test(){
        cout << "Sensors: " << singleGene.substr(sensorStart, sensorEnd) << endl
                << "Direction: " << singleGene.substr(directionStart, directionEnd) << endl
                << "Rotation: " << singleGene.substr(rotationStart, rotationEnd) << endl
                << "Actions: " << singleGene.substr(actionsStart, actionsEnd) << endl;
    }
    int sensorStart = 0, sensorEnd = 4;
    int directionStart = 4, directionEnd = 1;
    int rotationStart = 5, rotationEnd = 2;
    int actionsStart = 7, actionsEnd = 2;
public:
    string getSenorData(){
        return singleGene.substr(sensorStart, sensorEnd);
    }
    string getDirectionData(){
        return singleGene.substr(directionStart, directionEnd);
    }
    string getRotationData(){
        return singleGene.substr(rotationStart, rotationEnd);
    }
    string getActionsData(){
        return singleGene.substr(actionsStart, actionsEnd);
    }
    string singleGene;
    bool isSucessful;
    void init(){
        singleGene = "";
        isSucessful = false;
        if(debug){
            singleGene = "9221NRLMP";
            //test();
        }
    }
    void setGene(string gene){
        singleGene = gene;
        if(debug){
            //test();
        }
    }
};

class geneConstructor{
public:
    static string fullRandomGene(){
        string gene = "";
        for(int i = 0; i < 4; i++){
            gene += getRandomSensorData();
        }
        gene += getRandomDirection();
        gene += getRandomRotation();
        gene += getRandomAction();
        gene += getRandomAction(gene.at(gene.length() - 1));
        return gene;
    }
    static string getRandomSensorData(){
        int random = rand() % 3;
        switch(random){
            case 0:
                return "9";
            case 1:
                return "1";
            default:
                return "2";
        }
    }
    static string getRandomSensorData(char a){
        string returnData = "";
        do{
            int random = rand() % 3;
            switch(random){
                case 0:
                    returnData = "9";
                    break;
                case 1:
                    returnData = "1";
                    break;
                default:
                    returnData = "2";
                    break;
            }
        }while(returnData.at(0) == a);
        return returnData;
    }
    static string getRandomDirection(){
        int random = rand() % 4;
        switch(random){
            case 0:
                return "N";
            case 1:
                return "E";
            case 2:
                return "S";
            default:
                return "W";
        }
    }
    static string getRandomDirection(char a){
        string returnData;
        do{
            int random = rand() % 4;
            switch(random){
                case 0:
                    returnData = "N";
                    break;
                case 1:
                    returnData = "E";
                    break;
                case 2:
                    returnData = "S";
                    break;
                default:
                    returnData = "W";
                    break;
            }
        }while(returnData.at(0) == a);
        return  returnData;
    }
    static string getRandomRotation(){
        int random = rand() % 3;
        switch(random){
            case 0:
                return "RL";
            case 1:
                return "RR";
            default:
                return "NN";
        }
    }
    static string getRandomRotation(string a){
        string returnData;
        do{
            int random = rand() % 3;
            switch(random){
                case 0:
                    returnData = "RL";
                    break;
                case 1:
                    returnData = "RR";
                    break;
                default:
                    returnData = "NN";
                    break;
            }
        }while(returnData == a);
        return returnData;
    }
    static string getRandomAction(){
        int random = rand() % 3;
        switch(random){
            case 0:
                return "M";
            case 1:
                return "P";
            default:
                return "X";
        }
    }
    static string getRandomAction(char a){
        string returnData;
        do{
            int random = rand() % 3;
            switch(random){
                case 0:
                    returnData = "M";
                    break;
                case 1:
                    returnData = "P";
                    break;
                default:
                    returnData = "X";
                    break;
            }
        }while(returnData.at(0) == a);
        return returnData;
    }
};

class Robot{
public:
    struct testData{
        int structEnergy;
        int structx;
        int structy;
        int structTotalPoints;
        char structDirection;
    };
private:
    string getCurrentSensorData(){
        string currentSensor = "";
        if(map[x][y-1].hasBattery){
            currentSensor += "1";
        }else if(map[x][y-1].hasWall){
            currentSensor += "9";
        }else{
            currentSensor += "2";
        }

        if(map[x + 1][y].hasBattery){
            currentSensor += "1";
        }else if(map[x + 1][y].hasWall){
            currentSensor += "9";
        }else{
            currentSensor += "2";
        }

        if(map[x][y+1].hasBattery){
            currentSensor += "1";
        }else if(map[x][y+1].hasWall){
            currentSensor += "9";
        }else{
            currentSensor += "2";
        }

        if(map[x-1][y].hasBattery){
            currentSensor += "1";
        }else if(map[x-1][y].hasWall){
            currentSensor += "9";
        }else{
            currentSensor += "2";
        }
        return currentSensor;
    }
    int maxTurns = 12;
    void parser(){
        bool found = false;
        currentSensorData = getCurrentSensorData();
        for(int i = 0; i < 24; i++){
            if(currentSensorData == allGenes[i].getSenorData() && direction == allGenes[i].getDirectionData()){
                toTest = allGenes[i];
                found = true;
            }
        }
        if(found) {
            doRotate();
            doActions();
            if (maxTurns > 0 && energy > 0) {
                maxTurns--;
                energy--;
                if(oldx != x || oldy != y)
                    totalPoints++;
                parser();
            }
        }
    }
    singleGene toTest;
    void doRotate(){
        const char rotation[4] = {'N', 'E', 'S', 'W'};
        if(toTest.getRotationData() == "RL"){
            char currentDirection = toTest.getDirectionData().at(0);
            int currentDirectionIndex = 1;
            for(int i = 0; i < 4; i++){
                if(currentDirection == rotation[i]){
                    allActions.push_back("Rotate RL");
                    currentDirection == i;
                    break;
                }
            }
            currentDirection--;
            if(currentDirection == -1){
                currentDirection = 3;
                direction = rotation[currentDirection];
                allActions.push_back("Rotate RL 1");
            }else if(currentDirection == 4){
                currentDirection = 0;
                direction = rotation[currentDirection];
                allActions.push_back("Rotate RL 2");
            }else{
                direction = rotation[currentDirection];
                allActions.push_back("Rotate RL 3");
            }
        }else if(toTest.getRotationData() == "RR"){
            char currentDirection = toTest.getDirectionData().at(0);
            int currentDirectionIndex = 1;
            for(int i = 0; i < 4; i++){
                if(currentDirection == rotation[i]){
                    currentDirection == i;
                    break;
                }
            }
            currentDirection++;
            if(currentDirection == -1){
                currentDirection = 3;
                direction = rotation[currentDirection];
                allActions.push_back("Rotate RR 1");
            }else if(currentDirection == 4){
                currentDirection = 0;
                direction = rotation[currentDirection];
                allActions.push_back("Rotate RR 2");
            }else{
                direction = rotation[currentDirection];
                allActions.push_back("Rotate RR 3");
            }
        }else{
            //allActions.push_back("Rotate NN 1\n" +  + to_string(x) + " " + to_string(y) + "\n" + toTest.singleGene);
            direction = toTest.getDirectionData();
            //do nothing
        }
    }
    void doActions(){
        string actions = toTest.getActionsData();
        for(char x : actions){
            switch(x){
                case 'M':
                    allActions.push_back("About To doM()");
                    doM();
                    break;
                case 'P':
                    allActions.push_back("About to doP()");
                    doP();
                    break;
                default:
                    allActions.push_back("About to do nothing (X)");
                    //do Nothing
                    break;
            }
        }
    }
    void doM(){
        switch(direction.at(0)){
            case 'N':
                y--;
                if(y < 1){
                    y++;
                }
                allActions.push_back("Move 1");
                break;
            case 'E':
                x++;
                if(x > 10){
                    x = 10;
                }
                allActions.push_back("Move 2");
                break;
            case 'S':
                y++;
                if(y > 10){
                    y = 10;
                }
                allActions.push_back("Move 3");
                break;
            default:
                x--;
                if(x < 1){
                    x = 1;
                }
                allActions.push_back("Move 4");
                break;
        }
    }
    void doP(){
        if(map[x][y].hasBattery){
            allActions.push_back("Pick Up Battery");
            map[x][y].hasBattery = false;
            energy += 5;
        }
    }
public:
    vector<string> allActions;
    singleGene allGenes[24];
    int x, y, oldx, oldy, energy = 10, totalPoints = 0;
    string currentSensorData = "";
    string direction = "";

    friend bool operator<(const Robot &a, const Robot &b) {
        if(a.totalPoints < b.totalPoints){
            return true;
        }else{
            return false;
        }
    }
    friend bool operator!=(const Robot &a, const Robot &b){
        if(a.totalPoints == a.totalPoints){
            return false;
        }else{
            return true;
        }
    }
    friend int operator-(const Robot &a, const int &b){
        return 0;
    }
    friend int operator+(const Robot &a, const int &b){
        return 0;
    }
    Robot(){
        totalPoints = 0;
        x = rand() % 10 + 1;
        y = rand() % 10 + 1;
        oldx = x;
        oldy = y;
        direction = geneConstructor::getRandomDirection();
    }
    Robot(singleGene passGenes[24]){
        totalPoints = 0;
        x = rand() % 10 + 1;
        y = rand() % 10 + 1;
        oldx = x;
        oldy = y;
        direction = geneConstructor::getRandomDirection();
        for(int i = 0; i < 24; i++){
            allGenes[i] = passGenes[i];
        }
    }
    void randomiseAllGenes(){
        for(int i = 0; i < 24; i++){
            allGenes[i].init();
            allGenes[i].setGene(geneConstructor::fullRandomGene());
        }
    }
    void execute(){
        mapIndex oldMap[12][12];
        for(int i = 0; i < 12; i++){
            for(int j = 0; j < 12; j++){
                oldMap[i][j] = map[i][j];
            }
        }
        parser();
        for(int i = 0; i < 12; i++){ //undo all changes dont to map by last run
            for(int j = 0; j < 12; j++){
                map[i][j] = oldMap[i][j];
            }
        }
    }
};

void setMap(){
    for(int i = 0; i < 12; i++){
        map[i][0].hasWall = true;
        map[0][i].hasWall = true;
        map[i][11].hasWall = true;
        map[11][i].hasWall = true;
    }

    for(int i = 1; i < 11; i++){
        for(int j = 1; j < 11; j++){
            if((rand() % 100) < 40){
                map[i][j].hasBattery = true;
            }
        }
    }


    if(debug) {
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 12; j++) {
                if (map[i][j].hasWall)
                    cout << "1";
                else if (map[i][j].hasBattery)
                    cout << "2";
                else
                    cout << "0";
            }
            cout << endl;
        }
    }
}

bool lessThan(Robot &a, Robot &b){
    if(a.totalPoints < b.totalPoints){
        return true;
    }else
        return false;
}

void breed(vector<Robot> *allRobots){
    allRobots->erase(allRobots->begin(), allRobots->begin() + 100);
    for(int i = 0; i < 100; i++){
        if(i % 2 == 0){
            Robot robot;
            singleGene passGenes[24];

            for(int i = 0; i < 12; i++){
                passGenes[i] = allRobots->at(i).allGenes[i];
            }
            for(int i = 12; i < 24; i++){
                passGenes[i] = allRobots->at(i + 1).allGenes[i];
            }

            if((rand() % 10) < 4){
                int whichGene = rand() % 24;
                int whichPart = rand() % 9;
                if(whichPart == 5 || whichGene == 6){
                    passGenes[whichGene].singleGene.replace(5,6, geneConstructor::getRandomRotation(passGenes[whichGene].getRotationData()) + passGenes[whichGene].singleGene.substr(7,8));
                }else if(whichPart >= 0 && whichPart < 4){
                    passGenes[whichGene].singleGene.replace(whichPart, whichPart +1, geneConstructor::getRandomSensorData(passGenes[whichGene].getSenorData().at(whichPart)) + passGenes[whichGene].singleGene.substr(whichPart +1 ,8));
                    passGenes[whichGene].singleGene.resize(9);
                }else if(whichPart == 4){
                    passGenes[whichGene].singleGene.replace(whichPart, 1, geneConstructor::getRandomDirection(passGenes[whichGene].getDirectionData().at(0)));
                }else if(whichPart == 7 || whichPart == 8){
                    passGenes[whichGene].singleGene.replace(whichPart, 1, geneConstructor::getRandomDirection(passGenes[whichGene].singleGene.at(whichPart)));
                }
            }
            robot = Robot(passGenes);
            allRobots->push_back(robot);
        }else{
            Robot robot;
            singleGene passGenes[24];

            for(int i = 0; i < 12; i++){
                passGenes[i] = allRobots->at(i).allGenes[i];
            }
            for(int i = 12; i < 24; i++){
                passGenes[i] = allRobots->at(i - 1).allGenes[i];
            }
            robot = Robot(passGenes);
            allRobots->push_back(robot);
        }
    }
//    int input;
//    do {
//        cout << "Inter which index to show data about: ";
//        cin >> input;
//        for (string x : allRobots->at(input).allActions) {
//            cout << x << endl;
//        }
//    }while(input != -1);
}

int main(){
    srand(time(NULL)); //seed random
    int gen = 0;
    vector<Robot> *allRobots = new vector<Robot>(200);
    do {
        setMap();
        for (int i = 0; i < 200; i++) { //Generates robots and makes random genes
            allRobots->at(i) = Robot();
            allRobots->at(i).randomiseAllGenes();
        }
        for (int i = 0; i < 200; i++) {
            allRobots->at(i).execute();
        }
        std::sort(allRobots->begin(), allRobots->end());
        cout << "Best of generation " << gen << " is " << allRobots->at(199).totalPoints << "\t\tGen Pool: " << allRobots->size() << endl;
        gen++;
        breed(allRobots);
    }while(gen < 2000);
    return 0;
}